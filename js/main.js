let passwordForm = document.querySelector('.password-form');
passwordForm.addEventListener('submit', sendForm);

passwordForm.querySelectorAll('[type=password]').forEach(passwordField=>{
    passwordFieldsActions(passwordField);
})

function sendForm(e) {
    e.preventDefault();
    let form = e.target;
    let password = form.querySelector('[name=password]');
    let repeatPassword = form.querySelector('[name=repeatPassword]');
    removeAllErrors();
    let formData = new FormData(form);


    matchPassword(formData.get('password'), formData.get('repeatPassword'));


    function matchPassword(val1, val2) {
        if(val1.length === 0) {
            error('Значение не должно быть пустым', password);
            return ;
        }
        if(val1 === val2){
            msgSuccess('You are welcome');
            clearInput(password);
            clearInput(repeatPassword);
        }
        else{
            error('Нужно ввести одинаковые значения', repeatPassword);
            clearInput(repeatPassword);
        }

    }

    function msgSuccess(msg) {
        alert(msg);
    }

    function error(msg, after) {
        let parentEl = after.parentElement;
        if(!parentEl.querySelector('.error')){
            let error = document.createElement('p');
            error.textContent = msg;
            error.classList.add('error');
            error.style.cssText = "position: absolute; bottom: 5px; left:0; right: 0; text-align: center; margin: 0; color: red; ";
            parentEl.insertBefore(error, after.nextSibling);
        }

    }
     function removeAllErrors() {
         form.querySelectorAll('.error').forEach(error=>{
             error.remove();
         });
     }
     function clearInput(el){
        el.value = '';
     }

}


function passwordFieldsActions(field){
    let passwordIcon = field.parentElement.querySelector('.icon-password');
    if(!passwordIcon)
    return;
//block for display password
    let showPass = document.createElement('span');
    showPass.classList.add('enteringPassword');
    showPass.style.cssText = 'position: absolute; right: -10px; top: 12px; transform: translateX(100%);';




    function init() {
        field.dataset.password = 'hide';
        hidePasswordIcon();

    }
    init();

    passwordIcon.addEventListener('click', changeDisplayPassword);
    field.addEventListener('keyup', changePasswordVal);

    function changeDisplayPassword()
    {
        switch (field.dataset.password) {
            case 'hide':
                //then show
                field.dataset.password = 'show';
                field.setAttribute('type', 'text'); //просто дополнительно, можно и убрать
                showPass.textContent = field.value;
                passwordIcon.parentElement.append(showPass);
                showPasswordIcon();
                break;
            case 'show':
                //then hide
                field.dataset.password = 'hide';
                field.setAttribute('type', 'password');//просто дополнительно, можно и убрать
                showPass.remove();
                hidePasswordIcon();
                break;

        }
    }

    function changePasswordVal()
    {
        if(field.dataset.password == 'show'){
            showPass.textContent = field.value;
        }
    }
    function showPasswordIcon() {
        if(passwordIcon.classList.contains('fa-eye-slash'))
            passwordIcon.classList.replace('fa-eye-slash', 'fa-eye');

    }
    function hidePasswordIcon() {
        if(passwordIcon.classList.contains('fa-eye'))
            passwordIcon.classList.replace('fa-eye', 'fa-eye-slash');

    }


    // field.addEventListener('cl')
}